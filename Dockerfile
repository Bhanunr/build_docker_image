
FROM python:3.9.13
RUN pip install -U Flask 
COPY app.py /root/app.py
CMD ["python","/root/app.py"] 
